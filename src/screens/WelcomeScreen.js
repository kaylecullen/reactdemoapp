import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TouchableNativeFeedback } from 'react-native'
import { Container } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";

export class WelcomeScreen extends Component {

  static navigationOptions = ({navigation}) => ({
    header: null
  });

  render() {
    return (
      <Container>
        <View style={{flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontSize: 25, fontWeight: 'bold'}}>App Title</Text>
        </View>
        <View style={{flexDirection: 'row', display: 'flex', justifyContent: 'flex-end'}}>
          <Row>
            <Col style={{borderColor: '#000', borderWidth: 1, alignItems: 'center', padding: 30}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')} background={TouchableNativeFeedback.SelectableBackground()}>
                <View>
                  <Text>Login</Text>
                </View>
              </TouchableOpacity>
            </Col>
            <Col style={{borderColor: '#000', borderWidth: 1, alignItems: 'center', padding: 30}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('RegisterScreen')} background={TouchableNativeFeedback.SelectableBackground()}>
                <View>
                  <Text>Sign Up</Text>
                </View>
              </TouchableOpacity>
            </Col>
          </Row>
        </View>
      </Container>
    )
  }
}

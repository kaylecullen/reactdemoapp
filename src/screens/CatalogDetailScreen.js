import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Container, Left, Button, Card, CardItem, Item, Input } from 'native-base'
import Icon from "react-native-vector-icons/FontAwesome"

export class CatalogDetailScreen extends Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = ({navigation}) => ({
    headerLeft: (
      <Left>
        <Button transparent onPress={() => navigation.goBack()}>
          <Icon
            name="chevron-left"
            style={{ color: "#000", fontSize: 20, paddingLeft: 15 }}
          />
        </Button>
      </Left>
    ),
    headerTitle: 'Catalog Details'
  });
  render() {
    return (
      <Container>
        <View>
          <Card>
            <CardItem>
              <View style={{flex:1}}>
                <Item rounded>
                  <Icon active name='search'/>
                  <Input placeholder="Some text here"/>
                </Item>
                <Text style={{padding: 20, alignSelf: 'center', fontSize: 18, fontWeight:'bold'}}>Category Title</Text>
              </View>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <View style={{flex:1}}>
                <Item rounded>
                  <Icon active name='search'/>
                  <Input placeholder="Some text here"/>
                </Item>
                <Text style={{padding: 20, alignSelf: 'center', fontSize: 18, fontWeight:'bold'}}>Category Title</Text>
              </View>
            </CardItem>

          </Card>
        </View>
      </Container>
    )
  }
}

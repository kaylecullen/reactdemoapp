import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Container, Textarea, Item, Label, Input, Card, CardItem, Content } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { CatalogRow } from '../components/CatalogRow'
import { FlatList } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome";

import { SearchBar } from '../components/SearchBar'
export class CatalogScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      category: [
        {
          name: 'Category 1'
        },
        {
          name: 'Category 2'
        },
        {
          name: 'Category 3'
        },
        {
          name: 'Category 4'
        },
        {
          name: 'Category 5'
        },
        {
          name: 'Category 5'
        },
        {
          name: 'Category 5'
        },
        {
          name: 'Category 5'
        },
        {
          name: 'Category 5'
        },
        {
          name: 'Category 5'
        },
        {
          name: 'Category 5'
        },
      ]
    }
  }

  componentDidMount(){
    console.log(this.state.category)
  }

  render() {
    return (
     <Container>
       <Content>
          <View style={{margin:10,padding:0}}>
            <SearchBar/>
          </View>
          <View style={{margin:10}}>
            <FlatList
              data = {this.state.category}
              keyExtractor = {(item, i) => i.toString()}
              numColumns = {'2'}
              renderItem = {({item, index})=> (
                <CatalogRow
                  rowContent = {item}
                />
              )}
            />
          </View>
       </Content>
     </Container>
    )
  }
}

import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Container, Form, Item, Label, Input, Content, Button } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

export class LoginScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  render() {
    return (
      <Container>
        <Content>
          <View style={{display: 'flex',flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Icon
                style={{fontSize: 90, alignSelf: 'center', paddingVertical: 40}}
                name="user-circle"
              />
              <Text style={{fontSize: 20, fontWeight: 'bold', alignSelf: 'center'}}>
                LOGIN
              </Text>
          </View>
          <View style={{flex: 1, alignContent: 'flex-start', paddingHorizontal: 15}}>
            <Form style={{paddingStart:0, paddingBottom: 0, marginStart: 0, marginBottom: 0}}>
              <Item floatingLabel>
                <Label>Username</Label>
                <Input />  
              </Item>
              <Item floatingLabel>
                <Label>Password</Label>
                <Input />  
              </Item>
              {/* <TouchableOpacity>
                <View style={{display: 'flex', flexDirection: 'row', alignContent: 'center'}}>
                  <Icon name='cog' style={{fontSize: 14}}/>
                  <Text style={{fontSize: 14}}>Settings</Text>
                </View>
              </TouchableOpacity> */}
              <Button iconLeft transparent success 
                style={{alignSelf: 'center', padding: 50, margin: 30, borderColor: '#000', borderWidth: 1}}
                onPress={()=> this.props.navigation.navigate('HomeScreen')}>
                <Icon name='sign-in' style={{padding: 5}}/>
                <Text>LOGIN</Text>
              </Button>
            </Form>
          </View>
        </Content>
      </Container>
    );
  }
}

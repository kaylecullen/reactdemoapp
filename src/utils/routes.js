import React from "react";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome";
import { Left, Button } from "native-base";

//screens
import { CatalogScreen } from "../screens/CatalogScreen";
import { OrderScreen } from "../screens/OrderScreen";
import { WelcomeScreen } from "../screens/WelcomeScreen";
import { LoginScreen } from "../screens/LoginScreen";
import { RegisterScreen } from "../screens/RegisterScreen";
import { CatalogDetailScreen } from "../screens/CatalogDetailScreen";
//component
import { Sidebar } from "../components/Sidebar";
const AppStack = createStackNavigator(
  {
    HomeScreen: {
      screen: CatalogScreen
    },
    OrderScreen: {
      screen: OrderScreen
    },
    WelcomeScreen: {
      screen: WelcomeScreen
    },
    LoginScreen: {
      screen: LoginScreen
    },
    RegisterScreen: {
      screen: RegisterScreen
    },
    CatalogDetailScreen: {
      screen: CatalogDetailScreen
    }
  },
  {
    initialRouteName: "WelcomeScreen",
    navigationOptions: ({ navigation }) => ({
      headerLeft: (
        <Left>
          <Button transparent onPress={() => navigation.openDrawer()}>
            <Icon
              name="bars"
              style={{ color: "#000", fontSize: 20, paddingLeft: 15 }}
            />
          </Button>
        </Left>
      ),
      headerRight: (
        <Left>
          <Button transparent>
            <Icon
              name="shopping-cart"
              style={{ color: "#000", fontSize: 20, paddingRight: 15 }}
            />
          </Button>
        </Left>
      ),
      headerTitle: 'Title',
    })
  }
);

const Drawer = createDrawerNavigator(
  {
    App: {
      screen: AppStack
    }
  },
  {
    drawerPosition: "left",
    contentComponent: props => <Sidebar {...props} />
  }
);

export default () => <Drawer />;

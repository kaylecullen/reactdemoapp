import React, { Component } from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import { Card, Text } from 'native-base'

export default class CatalogRow extends Component {
  constructor(props){
    super(props)
  }

  componentDidMount(){
    console.log(this.props.rowContent)
  }
  render() {
    return (
      <TouchableWithoutFeedback>
        <Card style={{flex:1, padding: 30, justifyContent: 'center'}}>
          <Text>{this.props.rowContent.name}</Text>
        </Card>
      </TouchableWithoutFeedback>
    )
  }
}

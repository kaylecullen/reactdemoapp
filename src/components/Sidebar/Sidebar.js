import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  TouchableNativeFeedback,
  TouchableOpacity
} from "react-native";
import { Container, Content } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
export default class Sidebar extends Component {
  render() {
    return (
      <Container>
        <Content>
          <View style={{ flex: 1, alignSelf: "center", paddingTop: 10 }}>
            <Icon name="user-circle" style={{ padding: 20, fontSize: 70 }} />
            <Text style={{ paddingHorizontal: 20 }}>Username</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('HomeScreen')}
            background={TouchableNativeFeedback.SelectableBackground()}>
            <View style={{ padding: 20 }}>
              <Text>Menu</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('OrderScreen')}
            background={TouchableNativeFeedback.SelectableBackground()}>
            <View style={{ padding: 20 }}>
              <Text>My Order</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => console.log("sasas")}
            background={TouchableNativeFeedback.SelectableBackground()}>
            <View style={{ padding: 20 }}>
              <Text>Account</Text>
            </View>
          </TouchableOpacity>
        </Content>
        <TouchableOpacity
          onPress={() => console.log("sasas")}
          background={TouchableNativeFeedback.SelectableBackground()}>
          <View style={{ padding: 20 }}>
            <Text>Logout</Text>
          </View>
        </TouchableOpacity>
      </Container>
    );
  }
}

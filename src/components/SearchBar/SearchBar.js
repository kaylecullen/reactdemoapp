import React from 'react'
import { Item, Input } from 'native-base'
import Icon from "react-native-vector-icons/FontAwesome";

export const SearchBar = (props) => {
  return (
    <Item rounded style={{height:35}}>
      <Icon active name='search' size={15} style={{padding:10}}/>
      <Input placeholder='Search'/>
    </Item>
  );
}